<?php
/*

	VERSION 0.4  
		
    phpup is a tool that helps you upgrade your code to php5.x and fixes the
	
	<? to <?php
	
	and 
	
	<?= to <? echo
	
	
    Copyright (C) <2015>  <Yianis Giannopoulos>
	
	Contact at
	phpup@keacode.com

--------------------------

INSTRUCTIONS

On the zip file we have included a test_phpup.php and a test.txt ... Try to run phpup after you have unzip the phpup.zip to confirm that it works. If you wish to do that again, copy the code from test.txt

HOW DOES IT WORK

It simple scans the directory that the phpup.php is and upgrades all the code of .php files it will find. Theres no undo. PLEASE MAKE SURE THAT YOU BACK UP YOUR ORIGINAL CODE BEFORE RUNNING PHPUP.PHP

when you 

1) copy phpup.php to the folder that you want to upgrade your files.

there are two way to run phpup.php
( PLEASE MAKE SURE THAT YOU BACK UP YOUR ORIGINAL CODE BEFORE RUNNING PHPUP.PHP, theres no undo ! )

2.1) FROM BROWSER ... navigate to 

http://your.server.address/folder/phpup.php

2.2) FROM TERMINAL ... open a terminal and go to the folder that you want to upgrade your code, then run

php -f phpup.php 

3) remove the phpup.php or move the file to the next folder that you want to upgrade.

*/
?>
<pre>
<?php
$dir    = getcwd();
$files1 = scandir($dir);
//$files2 = scandir($dir, 1);

//print_r($files1);

foreach($files1	as $a)
{
	

		if (preg_match("/php/", $a) && $a!='phpup.php' && $a!='phpup.zip')
			{
				echo '
Processing: ' . $a;
	
				$data = file_get_contents("$dir/$a", true);
				$data = str_replace('.php','.php',$data);
			$data=str_replace('<?=', "<?php echo ",$data);
			$data=str_replace('<?php', "<?",$data);
			$data=str_replace('<?', "<?php",$data);			
//			$data=str_replace('mysql_query(',"mysqli_query($connection,",$data); // uncomment this if you would like to update your mysql queries as well.

			
			
			//echo $data;
			
				$new_file=str_replace('.php','.php',$a);
				file_put_contents($new_file, $data);
				echo "
	writing: $new_file";
				
	//break;
				
			}
}

?>

</pre>
Your files in this folder have been upgraded.